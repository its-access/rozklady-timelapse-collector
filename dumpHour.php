<?php
require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$yaml = Yaml::parse(file_get_contents(__DIR__.'/app/config/parameters.yml'));

$config = new \Doctrine\DBAL\Configuration();

$connectionParams = array(
    'dbname' => $yaml['parameters']['database_name'],
    'user' => $yaml['parameters']['database_user'],
    'password' => $yaml['parameters']['database_password'],
    'host' => $yaml['parameters']['database_host'],
    'driver' => 'pdo_pgsql',
);
$connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
$repo = new \ITS\MeasurementRepository($connection);

$data = $repo->getTimeframe(new \DateTime('2016-03-21 23:20'), new \DateTime('2016-03-21 23:40'));
file_put_contents('out.json', json_encode($data));
