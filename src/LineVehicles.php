<?php
namespace ITS;

use GuzzleHttp\ClientInterface;

class LineVehicles
{
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function getVehicles(string $line)
    {
        $response = $this->client->request('GET', 'CNR_GetVehicles', ['query' => ['r' => $line]]);
        $vehicles = (new \SimpleXMLElement($response->getBody()->getContents()))->p;
        $allVehicles = [];
        foreach ($vehicles as $vehicle) {
            $allVehicles[] = json_decode($vehicle, true);
        }
        return $allVehicles;
    }
}
