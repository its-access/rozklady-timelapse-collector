<?php
namespace ITS;

use Ramsey\Uuid\Uuid;

class DataCollector
{
    private $lineVehicles;
    private $repository;
    private $routeList;

    public function __construct(
        LineVehicles $lineVehicles,
        MeasurementRepository $repository,
        RouteList $routeList
    ) {
        $this->lineVehicles = $lineVehicles;
        $this->repository = $repository;
        $this->routeList = $routeList;
    }

    public function collect()
    {
        $lines = $this->routeList->readLineNumbers();
        $now = new \DateTime();
        $uuid = Uuid::uuid4();
        foreach ($lines as $line) {
            $data = $this->lineVehicles->getVehicles($line);
            foreach ($data as $vehicle) {
                $this->repository->insertData($vehicle, $now, $uuid);
            }
        }
    }
}
