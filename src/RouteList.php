<?php
namespace ITS;

use GuzzleHttp\ClientInterface;

class RouteList
{
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function readLineNumbers(): array
    {
        $res = $this->client->request('GET', 'GetRouteList', []);
        $data = json_decode($res->getBody()->getContents(), true);
        $lines = [];
        foreach (array_merge($data[0][1], $data[1][1]) as $key => $line) {
            if ($key % 2 === 0) {
                continue;
            }
            $lines[] = $line;
        }
        return $lines;
    }
}
