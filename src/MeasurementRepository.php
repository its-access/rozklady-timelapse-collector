<?php
namespace ITS;

use Doctrine\DBAL\Connection;
use Ramsey\Uuid\UuidInterface;

class MeasurementRepository
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function insertData(array $data, \DateTimeInterface $timestamp, UuidInterface $uuid)
    {
        $this->connection->insert('measurement', [
            'radio_nb' => $data[0],
            'nb' => $data[1],
            'line' => trim($data[2]),
            'variant' => $data[3],
            'direction' => $data[4],
            'course_id' => $data[5],
            'stops' => $data[6],
            'route_planned' => $data[7],
            'route_done' => $data[8],
            'longitude' => $data[9],
            'latitude' => $data[10],
            'prev_longitude' => $data[11],
            'prev_latitude' => $data[12],
            'difference' => $data[13],
            'state' => $data[15],
            'planned_start' => $data[16] === '' ? null : $data[16],
            'next_course_id' => $data[17],
            'next_planned_start' => $data[18] === '' ? null : $data[18],
            'next_line' => trim($data[19]),
            'next_variant' => $data[20],
            'next_direction' => $data[21],
            'sec_to_start' => $data[22],
            'vehicle_type' => $data[23],
            'notes' => $data[24],
            'current_display' => $data[25],
            'next_display' => $data[26],
            'timestamp' => $timestamp->format('Y-m-d H:i:s'),
            'series_id' => $uuid->toString()
        ]);
    }

    public function getTimeframe(\DateTimeInterface $start, \DateTimeInterface $end)
    {
        $stmnt = $this->connection->prepare('SELECT radio_nb, difference, state, vehicle_type, line, longitude, latitude, timestamp, series_id from measurement WHERE timestamp between :startTime and :endTime');
        $stmnt->bindValue('startTime', $start, 'datetime');
        $stmnt->bindValue('endTime', $end, 'datetime');
        $stmnt->execute();
        return $stmnt->fetchAll();
    }
}
