<?php
require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$yaml = Yaml::parse(file_get_contents(__DIR__.'/app/config/parameters.yml'));

$client = new \GuzzleHttp\Client(['base_uri' => 'http://rozklady.lodz.pl/Home/',]);

$routeList = new ITS\RouteList($client);

$config = new \Doctrine\DBAL\Configuration();
$connectionParams = array(
    'dbname' => $yaml['parameters']['database_name'],
    'user' => $yaml['parameters']['database_user'],
    'password' => $yaml['parameters']['database_password'],
    'host' => $yaml['parameters']['database_host'],
    'driver' => 'pdo_pgsql',
);

$connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

$repo = new \ITS\MeasurementRepository($connection);
$lineVehicles = new \ITS\LineVehicles($client);

$collector = new \ITS\DataCollector($lineVehicles, $repo, $routeList);
$collector->collect();
