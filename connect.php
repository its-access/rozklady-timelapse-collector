<?php
require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$yaml = Yaml::parse(file_get_contents(__DIR__.'/app/config/parameters.yml'));

echo(
    sprintf(
        "PGPASSWORD='%s' psql -h %s -U %s %s \n",
        $yaml['parameters']['database_password'],
        $yaml['parameters']['database_host'],
        $yaml['parameters']['database_user'],
        $yaml['parameters']['database_name']
    )
);
